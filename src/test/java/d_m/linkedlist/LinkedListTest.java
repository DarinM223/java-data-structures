package d_m.linkedlist;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class LinkedListTest {
    @org.junit.Test
    public void testToString() throws Exception {
        LinkedList<Integer> normList = LinkedList.create(2).add(3).add(4).build();
        assertEquals(normList.toString(), "2 -> 3 -> 4");

        LinkedList<Integer> oneElementList = LinkedList.create(2).build();
        assertEquals(oneElementList.toString(), "2");

        LinkedList<Integer> emptyList = LinkedList.empty();
        assertEquals(emptyList.toString(), "");
    }

    @org.junit.Test
    public void testReverse() throws Exception {
        LinkedList<Integer> list = LinkedList.create(2).add(3).add(4).build();

        assertEquals(list.reverse().toString(), "4 -> 3 -> 2");
    }

    @org.junit.Test
    public void testStack() throws Exception {
        Stack<Integer> stack = LinkedList.empty();
        stack.push(2);
        stack.push(3);
        stack.push(4);

        assertEquals(stack.peek().get(), new Integer(4));
        assertEquals(stack.pop().get(), new Integer(4));
        assertEquals(stack.pop().get(), new Integer(3));
        assertEquals(stack.peek().get(), new Integer(2));
        assertEquals(stack.pop().get(), new Integer(2));
    }

    @org.junit.Test(expected = NoSuchElementException.class)
    public void testEmptyStackPop() {
        Stack<Integer> stack = LinkedList.empty();
        stack.pop().get();
    }

    @org.junit.Test(expected = NoSuchElementException.class)
    public void testEmptyStackPeek() {
        Stack<Integer> stack = LinkedList.empty();
        stack.peek().get();
    }
}