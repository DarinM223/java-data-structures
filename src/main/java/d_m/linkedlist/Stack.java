package d_m.linkedlist;

import java.util.Optional;

public interface Stack<T> {
    void push(T data);
    Optional<T> pop();
    Optional<T> peek();
}
