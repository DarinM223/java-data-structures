package d_m.linkedlist;

import java.util.Optional;

public class LinkedList<T> implements Stack<T> {
    private Optional<Node<T>> root;

    private LinkedList() {
        this.root = Optional.empty();
    }

    private LinkedList(Node<T> root) {
        this.root = Optional.of(root);
    }

    /**
     * Starts building the LinkedList.
     * To add new data to the linked list call add()
     * and finish building with build().
     */
    public static <A> LinkedListBuilder<A> create(A data) {
        return new LinkedListBuilder<>(data);
    }

    /**
     * Creates an empty linked list
     */
    public static <A> LinkedList<A> empty() {
        return new LinkedList<>();
    }

    public static class LinkedListBuilder<A> {
        private Node<A> root;

        public LinkedListBuilder(A data) {
            this.root = new Node<>(data);
        }

        /**
         * Add a new node to the end of the linked list
         */
        public LinkedListBuilder<A> add(A data) {
            Optional<Node<A>> node = Optional.of(this.root);
            while (node.get().next.isPresent()) {
                node = node.get().next;
            }
            node.get().next = Optional.of(new Node<>(data));

            return this;
        }

        /**
         * Method to call when finishing creating the linked list
         */
        public LinkedList<A> build() {
            return new LinkedList<>(this.root);
        }
    }

    /**
     * Returns a string of the linked list given the root
     */
    public String toString() {
        if (this.root.isPresent()) {
            Optional<Node<T>> iterNode = this.root.get().next;

            StringBuilder builder = new StringBuilder();
            builder.append(this.root.get().toString());

            while (iterNode.isPresent()) {
                builder.append(" -> ");
                builder.append(iterNode.get().toString());
                iterNode = iterNode.get().next;
            }
            return builder.toString();
        } else {
            return "";
        }
    }

    /**
     * Reverses a linked list given the root.
     * This function mutates the inner list nodes so it
     * is not immutable.
     */
    public LinkedList<T> reverse() {
        Optional<Node<T>> prev = Optional.empty();
        Optional<Node<T>> curr = this.root;
        Optional<Node<T>> next;

        while (curr.isPresent()) {
            next = curr.get().next;
            curr.get().next = prev;
            prev = curr;
            curr = next;
        }

        return new LinkedList<>(prev.get());
    }

    /**
     * Adds a new node to the front of the linked list
     */
    public void push(T data) {
        Node<T> newNode = new Node<>(data);
        newNode.next = this.root;
        this.root = Optional.of(newNode);
    }

    /**
     * Removes the front node of the linked list and returns the data
     */
    public Optional<T> pop() {
        if (this.root.isPresent()) {
            Optional<T> data = Optional.of(this.root.get().data);
            this.root = this.root.get().next;
            return data;
        }
        return Optional.empty();
    }

    /**
     * Returns the data of the front node of the linked list
     */
    public Optional<T> peek() {
        if (this.root.isPresent()) {
            return Optional.of(this.root.get().data);
        }
        return Optional.empty();
    }
}
