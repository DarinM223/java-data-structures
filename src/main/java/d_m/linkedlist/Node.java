package d_m.linkedlist;

import java.util.Optional;

public class Node<T> {
    final T data;
    Optional<Node<T>> next;

    Node(T data) {
        this.data = data;
        this.next = Optional.empty();
    }

    @Override
    public String toString() {
        return this.data.toString();
    }
}
